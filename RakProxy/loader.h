#ifndef LOADER_H
#define LOADER_H

#include <fstream>
#include <string_view>
#include <windows.h>

extern std::ofstream flog;

#pragma pack( push, 1 )
struct socks5_ident_req {
	unsigned char Version = 5;
	unsigned char NumberOfMethods;
	unsigned char Methods[256];
};

struct socks5_ident_resp {
	unsigned char Version;
	unsigned char Method;
};

struct socks5_auth_req {
	char Version;
	char Method;
	char AuthData[1];
};

struct socks5_auth_resp {
	char Version;
	char Status;
};

struct socks5_req {
	unsigned char  Version;
	unsigned char  Cmd;
	unsigned char  Reserved;
	unsigned char  AddrType;
	in_addr		   IPv4;
	unsigned short DestPort;
};

struct socks5_resp {
	unsigned char  Version;
	unsigned char  Reply;
	unsigned char  Reserved;
	unsigned char  AddrType;
	in_addr		   IPv4;
	unsigned short BindPort;
};

struct socks5_udp_header {
	unsigned char  RSV[2]{ 0 };
	unsigned char  FRAG		= 0;
	unsigned char  AddrType = 1;
	in_addr		   IPv4;
	unsigned short DestPort;
};
#pragma pack( pop )

/**
 * \brief Выводит окно с сообщениемю
 * \detail Является оберткой над MessageBoxA, для более удобного вывода сообщений.
 * \param[in] text Текст сообщения.
 * \param[in] title Заголовок окна с сообщением.
 * \param[in] type Тип окна.
 * \return код завершения окна (нажатая кнопка).
 */
int MessageBox( std::string_view text, std::string_view title = _PRODJECT_NAME, UINT type = MB_OK );

#endif // LOADER_H
