#include "Socks5.h"
#include <stdexcept>

Socks5::Socks5( std::string_view host ) {
	auto sep = host.find( ':' );
	if ( sep == std::string::npos ) throw std::runtime_error( "invalid host" );
	address = host.substr( 0, sep );
	port	= std::atoi( host.substr( sep + 1 ).data() );

	flog << "Socks5 host: " << address << ":" << std::dec << port << std::endl;

	WORD	wVersion = MAKEWORD( 2, 2 );
	WSADATA wsaData;
	if ( 0 != ::WSAStartup( wVersion, &wsaData ) )
		throw std::runtime_error( "Can't initialize Winsock: " + std::to_string( ::WSAGetLastError() ) );

	tcp = ::socket( AF_INET, SOCK_STREAM, IPPROTO_TCP );
	if ( tcp == INVALID_SOCKET )
		throw std::runtime_error( "Can't create socket: " + std::to_string( ::WSAGetLastError() ) );

	flog << "Create TCP socket" << std::endl;
}

Socks5::~Socks5() {
	closesocket( tcp );
}

ConnectState Socks5::connect( std::string_view auth ) {
	if ( !auth.empty() ) {
		auto sep = auth.find( ':' );
		if ( sep == std::string::npos ) return ConnectState::AUTH_FAIL;
		return connect( auth.substr( 0, sep ), auth.substr( sep + 1 ) );
	}
	return connect( "", "" );
}

ConnectState Socks5::connect( std::string_view login, std::string_view password ) {
	sockaddr_in server;
	server.sin_addr.S_un.S_addr = ::inet_addr( address.data() );
	server.sin_family			= AF_INET;
	server.sin_port				= ::htons( port );
	if ( ::connect( tcp, (sockaddr *)&server, sizeof( server ) ) == SOCKET_ERROR ) {
		flog << "Can't connect to host: " << std::dec << ::WSAGetLastError() << std::endl;
		return ConnectState::CONNECTION_FAIL;
	}

	flog << "Connecting" << std::endl;

	char buffer[516]{ 0 }; // 516 - is biggest packet (auth)
	auto buflen = sizeof( buffer ) - 1;

	auto proxy_req			   = (socks5_ident_req *)buffer;
	proxy_req->Version		   = 5;
	proxy_req->NumberOfMethods = 1;
	proxy_req->Methods[0]	   = 0x00; // no auth

	if ( !login.empty() && !password.empty() ) {
		proxy_req->NumberOfMethods = 2;
		proxy_req->Methods[1]	   = 0x02; // login
	}
	auto proxy_req_len = 2 + proxy_req->NumberOfMethods;

	if ( ::send( tcp, buffer, proxy_req_len, 0 ) == SOCKET_ERROR ) {
		flog << "Can't send data to host: " << std::dec << ::WSAGetLastError() << std::endl;
		return ConnectState::SEND_FAIL;
	}

	::memset( buffer, 0, sizeof( buffer ) );
	if ( ::recv( tcp, buffer, buflen, 0 ) == SOCKET_ERROR ) {
		flog << "Can't recv data from host: " << std::dec << ::WSAGetLastError() << std::endl;
		return ConnectState::RECV_FAIL;
	}

	auto proxy_resp = (socks5_ident_resp *)buffer;
	if ( proxy_resp->Version != 5 ) {
		flog << "Invalid host socks version: " << std::dec << (int)proxy_resp->Version << ". Supported only SOCKSv5"
			 << std::endl;
		return ConnectState::VERSION_FAIL;
	}
	if ( proxy_resp->Method && proxy_resp->Method != 2 && proxy_resp->Method != 0xFF ) {
		flog << "Invalid host auth method: 0x" << std::hex << (int)proxy_resp->Method
			 << ". Supported methods: no auth, login" << std::endl;
		return ConnectState::AUTH_METHOD_FAIL;
	}

	auto auth_method = proxy_resp->Method;
	::memset( buffer, 0, sizeof( buffer ) );
	auto auth_req = (socks5_auth_req *)buffer;

	if ( auth_method == 0 ) {
		flog << "Auth: no auth" << std::endl;
		auth_req->Version = 5;
		auth_req->Method  = 0x00;
		//		if ( ::send( tcp, buffer, 2, 0 ) == SOCKET_ERROR ) {
		//			flog << "Can't send auth data to host: " << std::dec << ::WSAGetLastError() << std::endl;
		//			return ConnectState::SEND_FAIL;
		//		}
	} else if ( auth_method == 2 ) { // login
		flog << "Auth: login and pass" << std::endl;

		auth_req->Version	  = 5;
		auth_req->AuthData[0] = login.length();
		::memcpy( &auth_req->AuthData[1], login.data(), login.length() );
		flog << "Pass login: " << (char *)&auth_req->AuthData[1] << std::endl;
		auth_req->AuthData[login.length() + 1] = password.length();
		::memcpy( &auth_req->AuthData[login.length() + 2], password.data(), password.length() );
		flog << "Pass password: " << (char *)&auth_req->AuthData[login.length() + 2] << std::endl;
		auto auth_len = 3 + login.length() + password.length();

		if ( ::send( tcp, buffer, auth_len, 0 ) == SOCKET_ERROR ) {
			flog << "Can't send auth data to host: " << std::dec << ::WSAGetLastError() << std::endl;
			return ConnectState::SEND_FAIL;
		}
	} else if ( auth_method == 255 ) {
		flog << "No supported auth methods" << std::endl;
		return ConnectState::AUTH_FAIL;
		auth_req->Version = 5;
		auth_req->Method  = 0xFF;
		if ( ::send( tcp, buffer, 2, 0 ) == SOCKET_ERROR ) {
			flog << "Can't send auth data to host: " << std::dec << ::WSAGetLastError() << std::endl;
			return ConnectState::SEND_FAIL;
		}
	}
	if ( /*auth_method == 0 ||*/ auth_method == 2 /*|| auth_method == 255*/ ) {
		::memset( buffer, 0, sizeof( buffer ) );
		if ( ::recv( tcp, buffer, buflen, 0 ) == SOCKET_ERROR ) {
			flog << "Can't recv auth data from host: " << std::dec << ::WSAGetLastError() << std::endl;
			return ConnectState::RECV_FAIL;
		}

		auto auth_resp = (socks5_auth_resp *)buffer;
		if ( auth_resp->Version != 5 ) {
			flog << "Invalid host auth version: 0x" << std::hex << (int)auth_resp->Version << ". Supported only: 0x05"
				 << std::endl;
			return ConnectState::VERSION_FAIL;
		}
		if ( auth_resp->Status != 0x00 ) {
			flog << "Auth failed: " << std::dec << (int)auth_resp->Status << std::endl;
			return ConnectState::AUTH_FAIL;
		}
	}

	flog << "Connected" << std::endl;
	connected = true;
	return ConnectState::CONNECTED;
}

bool Socks5::isConnected() const {
	return connected;
}

const sockaddr_in &Socks5::proxyServer() const {
	return proxy;
}

std::string Socks5::host() const {
	return address + ":" + std::to_string( port );
}

SOCKET Socks5::createUdpTunnel( unsigned short port ) {
	if ( !isConnected() ) {
		flog << "Can't create tunnel before connecting" << std::endl;
		return SOCKET_ERROR;
	}

	flog << "Creating UDP tunnel" << std::endl;

	char buffer[12]{ 0 };
	auto buflen = sizeof( buffer ) - 1;

	if ( !udpInitialized ) {
		udpInitialized			  = true;
		auto cmd_req			  = (socks5_req *)buffer;
		cmd_req->Version		  = 5;
		cmd_req->Cmd			  = 0x03; // udp
		cmd_req->AddrType		  = 0x01; // IPv4
		cmd_req->IPv4.S_un.S_addr = 0;
		cmd_req->DestPort		  = ::htons( port );
		//		cmd_req->IPv4.S_un.S_addr = ::inet_addr( address.data() );
		//		cmd_req->DestPort		  = ::htons( this->port );

		if ( ::send( tcp, buffer, sizeof( socks5_req ), 0 ) == SOCKET_ERROR ) {
			flog << "Can't send cmd to host: " << std::dec << WSAGetLastError() << std::endl;
			return SOCKET_ERROR;
		}

		::memset( buffer, 0, sizeof( buffer ) );
		if ( ::recv( tcp, buffer, buflen, 0 ) == SOCKET_ERROR ) {
			flog << "Can't recv cmd from host: " << std::dec << WSAGetLastError() << std::endl;
			return SOCKET_ERROR;
		}

		auto cmd_resp = (socks5_resp *)buffer;

		if ( cmd_resp->Reply == 0x00 ) {
			flog << "Invalid reply from host: 0x" << std::hex << (int)cmd_resp->Reply << std::endl;
			return SOCKET_ERROR;
		}

		proxy.sin_family = AF_INET;
		//		proxy.sin_addr	 = cmd_resp->IPv4;
		//		proxy.sin_port	 = cmd_resp->BindPort;
		proxy.sin_addr.S_un.S_addr = ::inet_addr( address.data() );
		proxy.sin_port			   = ::htons( this->port );
	}

	SOCKET udp = ::socket( AF_INET, SOCK_DGRAM, IPPROTO_UDP );

	char optval[4];
	*(DWORD *)optval = 1;
	setsockopt( udp, SOL_SOCKET, SO_REUSEADDR, optval, 4 );
	*(DWORD *)optval = 0x40000;
	setsockopt( udp, SOL_SOCKET, SO_RCVBUF, optval, 4 );
	*(DWORD *)optval = 0x4000;
	setsockopt( udp, SOL_SOCKET, SO_SNDBUF, optval, 4 );
	u_long nonblocking = 1;
	ioctlsocket( udp, 0x8004667E, &nonblocking );
	setsockopt( udp, SOL_SOCKET, SO_BROADCAST, optval, 4 );

	SOCKADDR_IN addrSrv;
	addrSrv.sin_addr.s_addr = ::htonl( INADDR_ANY );
	addrSrv.sin_family		= AF_INET;
	addrSrv.sin_port		= ::htons( port );
	if ( ::bind( udp, (SOCKADDR *)&addrSrv, sizeof( SOCKADDR_IN ) ) == SOCKET_ERROR ) {
		flog << "Can't bind UDP socket" << std::endl;
		return SOCKET_ERROR;
	}

	flog << "UDP tunnel created!" << std::endl;
	return udp;
}

socks5_udp_header Socks5::createUdpHeader( std::string_view remoteIp, unsigned short remotePort ) {
	socks5_udp_header header;
	header.IPv4.S_un.S_addr = ::inet_addr( remoteIp.data() );
	header.FRAG				= 0;
	header.DestPort			= htons( remotePort );
	header.AddrType			= 0x01; // IPv4

	return header;
}
