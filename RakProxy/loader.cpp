#include "loader.h"
#include "Socks5.h"
#include <Nop.h>
#include <SRHook.hpp>
#include <callfunc.hpp>
#include <samp_pimpl.hpp>
#include <stdlib.h>
#include <winsock2.h>

std::ofstream flog;


BOOL APIENTRY DllMain( HMODULE, DWORD dwReasonForCall, LPVOID ) {
	static SRHook::Hook<char *, short>		connect( SAMP::isR1() ? 0x30640 : 0x339F0, 6, "samp" );
	static SRHook::Hook<short, int, char *> createBoundSocket( SAMP::isR1() ? 0x3ED08 : 0x420B8, 5, "samp" );
	static SRHook::Nop						nop_sendto( SAMP::isR1() ? 0x50027 : 0x533D7, 2, "samp" );
	static SRHook::Hook<SOCKET, char *, int, int, sockaddr *, int> send_to( SAMP::isR1() ? 0x50021 : 0x533D1, 6,
																			"samp" );
	static SRHook::Hook<SOCKET, char *, int, int, sockaddr *, int> recv_from( SAMP::isR1() ? 0x4FF62 : 0x53311,
																			  SAMP::isR1() ? 5 : 6, "samp" );

	if ( dwReasonForCall == DLL_PROCESS_ATTACH ) {

		flog.open( "RakProxy.log" );
		flog << "Start RakProxy" << std::endl;

		static std::string _host;
		static short	   _port = 0;
		static std::string _proxy_host;
		static short	   _proxy_port = 0;
		static Socks5 *	   _server	   = nullptr;
		;
		static socks5_udp_header _header;
		static char				 _send_buffer[0x4000];
		static char				 _recv_buffer[0x40000];

		// TODO: login and password in config
		try {
			std::ifstream proxy( "RakProxy.txt" );
			proxy >> _proxy_host;
			auto pos = _proxy_host.find( ':' );
			if ( pos == std::string::npos )
				proxy >> _proxy_port;
			else {
				_proxy_port = std::stoi( _proxy_host.substr( pos + 1 ) );
				_proxy_host.resize( pos, '\0' );
			}
			proxy.close();
		} catch ( ... ) {
			flog << "Can't read \"RakProxy.txt\"" << std::endl;
			flog.close();
			return FALSE;
		}


		connect.install( 4 );
		connect.onBefore += []( char *&host, short &port ) {
			// copy server address
			_host = CallFunc::stdcall<char *>( SAMP::Library() + ( SAMP::isR1() ? 0x4FED0 : 0x53280 ), host );
			_port = port;

			// do not hook sendto and recvfrom
			send_to.remove();
			nop_sendto.disable();
			recv_from.remove();

			// connect to proxy server
			if ( _server && !_server->isConnected() ) {
				delete _server;
				_server = nullptr;
			}
			if ( !_server ) _server = new Socks5( _proxy_host + ":" + std::to_string( _proxy_port ) );
			if ( !_server->isConnected() ) _server->connect( "log", "pass" );

			if ( _server->isConnected() )
				flog << "Connecting to " << host << ":" << std::dec << port << " via " << _proxy_host << ":" << std::dec
					 << _proxy_port << std::endl;
		};

		createBoundSocket.install();
		createBoundSocket.onBefore += []( short &localPort, int &blockingSocket, char *&forceHostAddress ) {
			if ( !_server ) return;
			if ( !_server->isConnected() ) return;

			auto fd = _server->createUdpTunnel( localPort );
			if ( fd == INVALID_SOCKET ) return;

			_header = _server->createUdpHeader( _host, _port );

			flog << "Connected via proxy!" << std::endl;
			createBoundSocket.skipOriginal();
			createBoundSocket.cpu.EAX = fd;
			createBoundSocket.cpu.ESP += 12;
			nop_sendto.enable();
			send_to.install();
			recv_from.install();
		};

		send_to.onAfter += []( SOCKET &fd, char *&buf, int &len, int &flags, sockaddr *&to, int &tolen ) {
			// assign header to buffer
			::memcpy( _send_buffer, &_header, sizeof( socks5_udp_header ) );
			::memcpy( _send_buffer + sizeof( socks5_udp_header ), buf, len );

			// sending data
			send_to.cpu.EAX = ::sendto( fd, _send_buffer, len + sizeof( socks5_udp_header ), flags,
										(sockaddr *)&_server->proxyServer(), tolen );

			// log errors
			if ( recv_from.cpu.EAX == SOCKET_ERROR && ::WSAGetLastError() )
				flog << "Failed to send 0x" << std::hex << len << " bytes: " << std::dec << ::WSAGetLastError()
					 << std::endl;

			// exit from hook
			send_to.cpu.ESP += 24;
		};

		recv_from.onBefore += []( SOCKET &fd, char *&buf, int &len, int &flags, sockaddr *&from, int &fromlen ) {
			recv_from.skipOriginal();
			int addr_len = sizeof( sockaddr_in );

			// recv data
			if ( SAMP::isR1() )
				recv_from.cpu.EAX = ::recvfrom( (SOCKET)buf, _recv_buffer, sizeof( _recv_buffer ), (int)from,
												(sockaddr *)&_server->proxyServer(), &addr_len );
			else
				recv_from.cpu.EAX = ::recvfrom( fd, _recv_buffer, sizeof( _recv_buffer ), flags,
												(sockaddr *)&_server->proxyServer(), &addr_len );

			// remove header from buffer
			if ( SAMP::isR1() )
				::memcpy( (char *)len, _recv_buffer + sizeof( socks5_udp_header ), flags );
			else
				::memcpy( buf, _recv_buffer + sizeof( socks5_udp_header ), len );

			// exit from hook
			recv_from.cpu.ESP += 24;
			if ( SAMP::isR1() ) recv_from.cpu.ESP += 4;
		};

	} else if ( dwReasonForCall == DLL_PROCESS_DETACH ) {
		flog << "Shutdown RakProxy" << std::endl;
		send_to.remove();
		nop_sendto.disable();
		createBoundSocket.remove();
		connect.remove();
		flog.close();
	}

	return TRUE;
}

int MessageBox( std::string_view text, std::string_view title, UINT type ) {
	return MessageBoxA( 0, text.data(), title.data(), type );
}
