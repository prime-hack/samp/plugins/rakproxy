#ifndef SOCKS5_H
#define SOCKS5_H

#include "loader.h"

enum class ConnectState { CONNECTED, CONNECTION_FAIL, SEND_FAIL, RECV_FAIL, VERSION_FAIL, AUTH_METHOD_FAIL, AUTH_FAIL };

class Socks5 {
	SOCKET		   tcp;
	std::string	   address;
	unsigned short port;
	bool		   connected	  = false;
	bool		   udpInitialized = false;
	sockaddr_in	   proxy;

public:
	Socks5( std::string_view host );
	~Socks5();

	ConnectState connect( std::string_view auth );
	ConnectState connect( std::string_view login = std::string_view(), std::string_view password = std::string_view() );

	bool			   isConnected() const;
	const sockaddr_in &proxyServer() const;
	std::string		   host() const;

	SOCKET			  createUdpTunnel( unsigned short port );
	socks5_udp_header createUdpHeader( std::string_view remoteIp, unsigned short remotePort );
};

#endif // SOCKS5_H
